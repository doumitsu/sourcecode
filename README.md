#### 採用ご担当者様
お世話になっております。

この度、データマイニングチームに応募させていただきました堂満竜明です。

以下、アップロードさせていただいたソースコードについて簡単に説明させていただきます。
<br><br>
#### １．多変量解析
私が学生のときに、学習と研究のために書きました。

私が所属していた研究室のルールのため、マイナーですがDelphi言語を用いています。

教科書通りの内容に過ぎませんが、私自身の勉強になりました。

##### 分析の主コード
[・主成分分析.pas](https://gitlab.com/doumitsu/sourcecode/-/blob/main/%E5%A4%9A%E5%A4%89%E9%87%8F%E8%A7%A3%E6%9E%90/%E4%B8%BB%E6%88%90%E5%88%86%E5%88%86%E6%9E%90.pas)

[・因子分析.pas](https://gitlab.com/doumitsu/sourcecode/-/blob/main/%E5%A4%9A%E5%A4%89%E9%87%8F%E8%A7%A3%E6%9E%90/%E5%9B%A0%E5%AD%90%E5%88%86%E6%9E%90.pas)

[・クラスター分析.pas](https://gitlab.com/doumitsu/sourcecode/-/blob/main/%E5%A4%9A%E5%A4%89%E9%87%8F%E8%A7%A3%E6%9E%90/%E3%82%AF%E3%83%A9%E3%82%B9%E3%82%BF%E3%83%BC%E5%88%86%E6%9E%90.pas)

[・多次元尺度法.pas](https://gitlab.com/doumitsu/sourcecode/-/blob/main/%E5%A4%9A%E5%A4%89%E9%87%8F%E8%A7%A3%E6%9E%90/%E5%A4%9A%E6%AC%A1%E5%85%83%E5%B0%BA%E5%BA%A6%E6%B3%95.pas)

##### 上記コードから利用されるサブコード
[・ヤコビ法.pas](https://gitlab.com/doumitsu/sourcecode/-/blob/main/%E5%A4%9A%E5%A4%89%E9%87%8F%E8%A7%A3%E6%9E%90/%E3%83%A4%E3%82%B3%E3%83%93%E6%B3%95.pas)

[・クラスター距離計算.pas](https://gitlab.com/doumitsu/sourcecode/-/blob/main/%E5%A4%9A%E5%A4%89%E9%87%8F%E8%A7%A3%E6%9E%90/%E3%82%AF%E3%83%A9%E3%82%B9%E3%82%BF%E3%83%BC%E8%B7%9D%E9%9B%A2%E8%A8%88%E7%AE%97.pas)
<br><br>
#### ２．機械学習
入社の応募時と同じ内容ですが、今回の求人でも関係あるかと思いますので、

再度アップロードさせていただきました。

私の現職の日本酒造りに関する、酒造工程のデータを前もって予測するために書きました。

Pythonのscikit-learnを用いた、教師あり学習のコードです。

##### 分析の主コード
[・線形回帰モデル.py](https://gitlab.com/doumitsu/sourcecode/-/blob/main/%E6%A9%9F%E6%A2%B0%E5%AD%A6%E7%BF%92/%E7%B7%9A%E5%BD%A2%E5%9B%9E%E5%B8%B0%E3%83%A2%E3%83%87%E3%83%AB.py)

##### 上記コードから利用されるサブコード
[・data_input.py（データ入力用）](https://gitlab.com/doumitsu/sourcecode/-/blob/main/%E6%A9%9F%E6%A2%B0%E5%AD%A6%E7%BF%92/data_input.py)

##### サンプルデータ
[・サンプルデータ.xlsx](https://gitlab.com/doumitsu/sourcecode/-/blob/main/%E6%A9%9F%E6%A2%B0%E5%AD%A6%E7%BF%92/%E3%82%B5%E3%83%B3%E3%83%97%E3%83%AB%E3%83%87%E3%83%BC%E3%82%BF.xlsx)

<br>

サンプルデータ.xlsxの内容を表計算ソフトで全部（該当ファイルではA1～D81まで）クリップボードにコピーした状態で、

線形回帰モデル.pyを実行すると分析がされるようになっています。

サンプルデータの一番右の列を目的変数、それ以外を説明変数としています。

<br>

グリッドサーチで最も良かったパラメータやそのときの平均スコアを出力し、

そのときの特徴量ごとの重要度のグラフや、パラメータごとの平均スコアのグラフが描画されるようになっています。

（私はAnacondaのSpyderでグラフを表示していました。）

<br>

線形モデル以外のk-最近傍法や決定木などのコードも作りましたが、

似たような内容ですので割愛いたしました。

<br>

以上、拙いコードと説明にて恐縮ですが、ご査収の程よろしくお願いいたします。

堂満竜明
