# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 23:42:49 2018

@author: Doumitsu
"""

import pandas as pd
import numpy as np
from sklearn.preprocessing import PolynomialFeatures


def data_input():
    #ワンホットエンコーディングを利用したデータのインプット
    df = pd.read_clipboard(engine='python')
    param_num = len(df.columns) #目的変数を含めた全変数の数
    X_df = df.iloc[:, :param_num-1]
    y_df = df.iloc[:, param_num-1:]
    
    X_df_dummies = pd.get_dummies(X_df)
    X = X_df_dummies.values
    
    poly = PolynomialFeatures(degree=1, include_bias=False)
    poly.fit(X)
    X = poly.transform(X)
    
    
    length = len(X[0]) + 1 #ダミー変数分と多項式展開分も加えた全変数の数
    np_X_names = X_df_dummies.columns.values
    
    y = y_df.values
    y = np.reshape(y, (-1))
    
    
    #多項式展開したときの、特徴量の名前がX0などになってしまうので、日本語に直す
    poly_np_X_names = []
    for strg in poly.get_feature_names():
        for i in range(len(X_df_dummies.columns)):
            param_strg = "x" + str(i)
            strg = strg.replace(param_strg, np_X_names[i]) #x[i]となっている部分をその変数名にする
        poly_np_X_names.append(strg)
    np_X_names = poly_np_X_names #多項式展開の次数が１なら変化なし。
    
   
    print("説明変数： {}".format(np_X_names))
    print("目的変数： {}".format(y_df.columns.values))
    
    return X, y, length, np_X_names
