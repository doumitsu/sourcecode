# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 15:43:27 2018

@author: Doumitsu
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import Ridge
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import ShuffleSplit
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
import seaborn as sns
from data_input import data_input


#データ入力
X, y, length, np_X_names = data_input()


#パイプラインの設定
pipe = Pipeline([("preprocessing", StandardScaler()), ("ridge", Ridge())])
#パラメータグリッドの設定
alpha_settings = [0.01, 0.0316, 0.1, 0.316, 1, 3.16, 10, 31.6, 100]
log_alpha_settings = [-2, -1.5, -1, -0.5, 0, 0.5, 1, 1.5,  2]
param_grid = {'preprocessing': [None, StandardScaler()], 'ridge__alpha': alpha_settings}
#交差検証の設定
shuffle_split = ShuffleSplit(test_size=0.25, train_size=0.75, n_splits=50)


#グリッドサーチの実行
grid_search = GridSearchCV(pipe, param_grid, cv=shuffle_split, scoring="r2", \
                           return_train_score=True)
grid_search.fit(X, y)
print("Best parameters: {}".format(grid_search.best_params_))
print("Best cross-validation score: {:.2f}".format(grid_search.best_score_))
print("Best coefficient\n: {}".format(grid_search.best_estimator_.named_steps["ridge"].coef_))
print("Best intercept\n: {}".format(grid_search.best_estimator_.named_steps["ridge"].intercept_))


#特徴量ごとの重要度（傾き）を描画
sns.set(font=['IPAPGothic'])
fig, ax = plt.subplots(1, 1, figsize=(8, 10))
ax.barh(range(length - 1), grid_search.best_estimator_.named_steps["ridge"].coef_, align='center')
plt.yticks(np.arange(length - 1), np_X_names[:length])
plt.xlabel("Feature importance")
plt.ylabel("Feature")


#パラメータごとの平均スコアを描画
results = pd.DataFrame(grid_search.cv_results_) #データフレームに変換
fig, ax = plt.subplots(1, 1, figsize=(8, 4))
ax.plot(log_alpha_settings, results.mean_test_score[0:9], label='None', c='b')
ax.plot(log_alpha_settings, results.mean_test_score[9:18], label='Standard Scaler', c='g')
plt.xlabel("alpha settings")
plt.ylabel("cross validation scores")
ax.legend()
