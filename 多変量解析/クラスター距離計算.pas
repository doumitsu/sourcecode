unit CalcDistance;

interface

(*データ間距離の計算*)
function pls_range(in_method, //0 : ユークリッド距離
                              //1 : ユークリッド距離の２乗
                   in_i,   //データ番号
                   in_j,   //データ番号
                   in_prmN //変数の数
                   : integer;
                   in_dat  //データ
                   : array of double
                   ) : double; //return r : 距離


(*２つのクラスター間の距離*)
function pls_range_c(in_method,         // = 1 最短距離法
                                        // = 2 最長距離法
                                        // = 3 メジアン法
                                        // = 4 重心法
                                        // = 5 郡平均法
                                        // = 6 ウォード法　
                    in_Dio,             //クラスターiとクラスターoとの距離
                    in_Djo,             //クラスターjとクラスターoとの距離
                    in_Dij   :  double; //クラスターiとクラスターjとの距離
                    in_ni,              //クラスターiに含まれるデータ数
                    in_nj,              //クラスターjに含まれるデータ数
                    in_no    :  integer)//クラスターoに含まれるデータ数
                             :  double; // return r1 : 距離

implementation

(*データ間距離の計算*)
function pls_range(in_method, //0 : ユークリッド距離
                              //1 : ユークリッド距離の２乗
                   in_i,   //データ番号
                   in_j,   //データ番号
                   in_prmN //変数の数
                   : integer;
                   in_dat  //データ
                   : array of double
                   ) : double; //return r : 距離
var
  x1, r1 : double;
  lop3 : integer;
begin
  r1 := 0;
  for lop3 := 0 to in_prmN - 1 do begin
    x1 := in_dat[in_prmN * in_i + lop3] - in_dat[in_prmN * in_j + lop3];
    r1 := r1 + (x1 * x1);
  end;
  if in_method = 0 then
    r1 := sqrt(r1);
  result := r1;
end;

(*２つのクラスター間の距離*)
function pls_range_c(in_method,         // = 1 最短距離法
                                        // = 2 最長距離法
                                        // = 3 メジアン法
                                        // = 4 重心法
                                        // = 5 郡平均法
                                        // = 6 ウォード法　
                    in_Dio,             //クラスターiとクラスターoとの距離
                    in_Djo,             //クラスターjとクラスターoとの距離
                    in_Dij   :  double; //クラスターiとクラスターjとの距離
                    in_ni,              //クラスターiに含まれるデータ数
                    in_nj,              //クラスターjに含まれるデータ数
                    in_no    :  integer)//クラスターoに含まれるデータ数
                             :  double; // return r1 : 距離
var
  r1 : double;
  nk : integer;
begin
  r1 := 0;
  //最短距離法
  if in_method = 1 then begin
    if in_Dio <= in_Djo then
      r1 := in_Dio;
    if in_Djo < in_Dio then
      r1 := in_Djo;
  end;
  //最長距離法
  if in_method = 2 then begin
    if in_Dio >= in_Djo then
      r1 := in_Dio;
    if in_Djo > in_Dio then
      r1 := in_Djo;
  end;
  //メジアン法
  if in_method = 3 then
    r1 := 0.5 * in_Dio + 0.5 * in_Djo - 0.25 * in_Dij;
  //重心法
  if in_method = 4 then begin
    nk := in_ni + in_nj;
    r1 := (in_ni * in_Dio / nk) + (in_nj * in_Djo / nk) - (in_ni * in_nj * in_Dij) / (nk * nk);
  end;
  //郡平均法
  if in_method = 5 then begin
    nk := in_ni + in_nj;
    r1 := (in_ni * in_Dio / nk) + (in_nj * in_Djo / nk);
  end;
  //ウォード法
  if in_method = 6 then begin
    nk := in_ni + in_nj;
    r1 := ((in_ni + in_no) * in_Dio + (in_nj + in_no) * in_Djo - in_no * in_Dij)
          / (nk + in_no);
  end;
  result := r1;
end;

end.
