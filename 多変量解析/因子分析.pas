unit FactorAnalysis;

interface

uses
  JacobiMethod;

(*主因子法による因子分析*)
function pls_FactorAnalysis(in_datN,     //データの数
                            in_prmN,     //変数の数
                            in_fctN      //共通因子負荷量の数
                            : integer;
                            in_dat       //データ
                            : array of double;
                       var  out_iv,      //固有値
                            out_A,       //係数(因子負荷量)
                                         //（行：変数番号、列：因子番号）
                            out_h        //各変数の共通性
                            : array of double;
                            in_eps       //収束性を判定する規準
                            : double;
                            in_ct        //最大繰り返し回数
                            : integer
                            ) : integer; //return =0 : 正常
                                         //       =1 : エラー

const
  prmmax = 50;

var

  C,                //分散共分散行列
  X                 //列が固有ベクトルの行列
   : array[0..(prmmax - 1) * (prmmax - 1)] of double;
  R,                //固有値
  tmp_h : array[0..prmmax - 1] of double;
  s : array[0..prmmax - 1] of integer;

implementation


(*主因子法による因子分析*)
function pls_FactorAnalysis(in_datN,     //データの数
                            in_prmN,     //変数の数
                            in_fctN      //共通因子負荷量の数
                            : integer;
                            in_dat       //データ
                            : array of double;
                       var  out_iv,      //固有値
                            out_A,       //係数(因子負荷量)
                                         //（行：変数番号、列：因子番号）
                            out_h        //各変数の共通性
                            : array of double;
                            in_eps       //収束性を判定する規準
                            : double;
                            in_ct        //最大繰り返し回数
                            : integer
                            ) : integer; //return =0 : 正常
                                         //       =1 : エラー
var
  lop1, lop2, lop3, max_i, count, sw : integer;
  mean,          //平均
  s2,            //標準偏差
  max_d : double;
begin
  //データの標準化//
  for lop1 := 0 to in_prmN - 1 do begin
    mean := 0;
    s2 := 0;
    for lop2 := 0 to in_datN - 1 do begin
      mean := mean + in_dat[in_prmN * lop2 + lop1];
      s2 := s2 + (in_dat[in_prmN * lop2 + lop1]
                  * in_dat[in_prmN * lop2 + lop1]);
    end;
    mean := mean / in_datN;
    s2 := s2 / in_datN;
    s2 := in_datN * (s2 - mean * mean) / (in_datN - 1);
    s2 := sqrt(s2);
    for lop2 := 0 to in_datN - 1 do
      in_dat[in_prmN * lop2 + lop1]
      := (in_dat[in_prmN * lop2 + lop1] - mean) / s2;
  end;
  //分散共分散行列→因子行列の計算//
  for lop1 := 0 to in_prmN - 1 do begin
    for lop2 := 0 to in_prmN - 1 do begin
      s2 := 0;
      for lop3 := 0 to in_datN - 1 do
        s2 := s2 + (in_dat[in_prmN * lop3 + lop1]
                    * in_dat[in_prmN * lop3 + lop2]);
      s2 := s2 / (in_datN - 1);
      C[in_prmN * lop1 + lop2] := s2;
      if lop1 <> lop2 then
        C[in_prmN * lop2 + lop1] := s2;
    end;
  end;
  //共通性の初期値//
  for lop1 := 0 to in_prmN - 1 do
    tmp_h[lop1] := 1;
  count := 0;
  sw := 1;
  while (count < in_ct) and (sw > 0) do begin

    //分散共分散行列の固有値と固有ベクトルの計算（ヤコビ法）//
    sw := pls_JacobiMethod(in_prmN, in_ct, in_eps, C, R, X);

    if sw = 0 then begin
      //共通性および共通因子負荷量の計算//
      //固有値の大きい順に選ぶ//
      for lop1 := 0 to in_prmN - 1 do
        s[lop1] := 0;          //既に選ばれた(1)か選ばれてないか(0)
      for lop1 := 0 to in_fctN - 1 do begin
        max_i := -1;
        max_d := 0;
        for lop2 := 0 to in_prmN - 1 do begin
          if (s[lop2] = 0) and ((max_i < 0) or (R[lop2] > max_d)) then begin
            max_i := lop2;
            max_d := R[lop2];
          end;
        end;
        s[max_i] := 1;
        out_iv[lop1] := R[max_i];
        s2 := sqrt(out_iv[lop1]);
        for lop2 := 0 to in_prmN - 1 do
          out_A[in_prmN * lop1 + lop2]
            := s2 * X[in_prmN * lop2 + max_i]; //第(lop1)因子負荷量
      end;
      for lop1 := 0 to in_prmN - 1 do begin
        out_h[lop1] := 0;
        for lop2 := 0 to in_fctN - 1 do
          out_h[lop1] := out_h[lop1] + (out_A[in_prmN * lop2 + lop1]
                                      * out_A[in_prmN * lop2 + lop1]);
          //(lop1)番目の変数の共通性
      end;
      for lop1 := 0 to in_fctN - 1 do begin
        if abs(out_h[lop1] - tmp_h[lop1]) > in_eps then
          sw := 1;
        if sw <> 0 then
          break;
      end;
      //収束しない場合//
      if sw > 0 then begin
        inc(count);
        for lop1 := 0 to in_prmN - 1 do begin
          tmp_h[lop1] := out_h[lop1];
          C[in_prmN * lop1 + lop1] := tmp_h[lop1];
        end;
      end;
    end;
  end;
  result := sw;
end;

end.
