unit PrincipleComponentAnalysis;

interface

uses
  JacobiMethod;

const
  prmmax = 50;     //入力パラメータ数の上限
  datmax = 500;   //入力データ数の上限

var
  C    //分散共分散行列
   : array[0..(prmmax - 1) * (prmmax - 1)] of double;
  mean_arr : array[0..prmmax - 1] of double;  //平均の配列

(*主成分分析*)
function  pls_PrincipleComponentAnalysis(in_prmN,     //変数の数
                                         in_datN      //データの数
                                         : integer;
                                         in_dat       //入力データ(行：データ、列：変数の番号を表す)
                                         : array of double;
                                     var out_dat_result, //出力データ
                                         out_r,       //分散（主成分）
                                         out_a        //係数(行：変数、列：固有ベクトルの番号を表す)
                                         : array of double;
                                         in_eps       //収束性を判定する基準
                                         : double;
                                         in_ct        //最大繰り返し回数
                                         : integer;
                                         in_normalize //false 標準化しない
                                                      //true 標準化する
                                         : boolean
                                         ) : integer; //return =0 正常
                                                      //       =1 エラー

implementation


(*主成分分析*)
function  pls_PrincipleComponentAnalysis(in_prmN,     //変数の数
                                         in_datN      //データの数
                                         : integer;
                                         in_dat       //入力データ(行：データ、列：変数の番号を表す)
                                         : array of double;
                                     var out_dat_result, //出力データ
                                         out_r,       //分散（主成分）
                                         out_a       //係数(行：変数、列：固有ベクトルの番号を表す)
                                         : array of double;
                                         in_eps       //収束性を判定する基準
                                         : double;
                                         in_ct        //最大繰り返し回数
                                         : integer;
                                         in_normalize //false 標準化しない
                                                      //true 標準化する
                                         : boolean
                                         ) : integer; //return =0 正常
                                                      //       =1 エラー
var
  mean,  //平均値
  s2,    //標準偏差
  Rtmp, Atmp : double;
  lop1, lop2, lop3, sw : integer;
begin

  {標準化する場合、データを平均値０、分散１に標準化してから
  分散共分散行列を計算}
  if in_normalize = true then begin
    for lop1 := 0 to in_prmN - 1 do begin
      mean := 0;
      s2 := 0;
      for lop2 := 0 to in_datN - 1 do begin
        mean := mean + in_dat[in_prmN * lop2 + lop1];
        s2 := s2 + ((in_dat[in_prmN * lop2 + lop1]
                  * in_dat[in_prmN * lop2 + lop1]));
      end;
      mean := mean / in_datN;
      s2 := s2 / in_datN;
      s2 := in_datN * (s2 - mean * mean) / (in_datN - 1);
      //普通の平均を求めるときはｎで割るけど、分散を求めるときはn-1で割る？
      s2 := sqrt(s2);
      for lop2 := 0 to in_datN - 1 do
        in_dat[in_prmN * lop2 + lop1] := (in_dat[in_prmN * lop2 + lop1] - mean) / s2;
    end;
    //分散共分散行列の計算
    for lop1 := 0 to in_prmN - 1 do begin
      for lop2 := lop1 to in_prmN - 1 do begin
        s2 := 0;
        for lop3 := 0 to in_datN - 1 do
          s2 := s2 + in_dat[in_prmN * lop3 + lop1] * in_dat[in_prmN * lop3 + lop2];
        s2 := s2 / (in_datN - 1);
        C[in_prmN * lop1 + lop2] := s2;
        if lop1 <> lop2 then
          C[in_prmN * lop2 + lop1] := s2;
      end;
    end;
  end;
  {標準化しない場合、各変数の平均値を計算してから、分散共分散行列を計算する}
  if in_normalize = false then begin
    for lop1 := 0 to in_prmN - 1 do begin
      mean_arr[lop1] := 0;
      for lop2 := 0 to in_datN - 1 do begin
        mean_arr[lop1] := mean_arr[lop1] + in_dat[in_prmN * lop2 + lop1];
        mean_arr[lop1] := mean_arr[lop1] / in_datN;
      end;
    end;
    //分散共分散行列の計算
    for lop1 := 0 to in_prmN - 1 do begin
      for lop2 := 0 to in_prmN - 1 do begin
        s2 := 0;
        for lop3 := 0 to in_datN - 1 do
          s2 := s2 + ((in_dat[in_prmN * lop3 + lop1] - mean_arr[lop1])
                   * (in_dat[in_prmN * lop3 + lop2] - mean_arr[lop2]));
        s2 := s2 / (in_datN - 1);
        C[in_prmN * lop1 + lop2] := s2;
        if lop1 <> lop2 then
          C[in_prmN * lop2 + lop1] := s2;
      end;
    end;
  end;

  //固有値と固有ベクトルの計算（ヤコビ法）
  sw := pls_JacobiMethod(in_prmN, in_ct, in_eps, C, out_r, out_a);

  if sw = 0 then begin
    //昇順並び替え//
    for lop1 := 0 to in_prmN - 2 do begin
      for lop2 := lop1 + 1 to in_prmN - 1 do begin
        if out_r[lop2] > out_r[lop1] then begin
          Rtmp := out_r[lop1];
          out_r[lop1] := out_r[lop2];
          out_r[lop2] := Rtmp;
          //固有ベクトルの番号を表す（列）方を入れ替える
          for lop3 := 0 to in_prmN - 1 do begin
            Atmp := out_a[in_prmN * lop3 + lop1];
            out_a[in_prmN * lop3 + lop1] := out_a[in_prmN * lop3 + lop2];
            out_a[in_prmN * lop3 + lop2] := Atmp;
          end;
        end;
      end;
    end;
    //回転後のデータの計算//
    for lop1 := 0 to in_datN - 1 do begin
      for lop2 := 0 to in_prmN - 1 do begin
        out_dat_result[in_prmN * lop1 + lop2] := 0;
        for lop3 := 0 to in_prmN - 1 do
          out_dat_result[in_prmN * lop1 + lop2] := out_dat_result[in_prmN * lop1 + lop2]
                                             + (in_dat[in_prmN * lop1 + lop3]
                                             * out_a[in_prmN * lop3 + lop2]);
      end;
    end;
  end;
  result := sw;
end;

end.
