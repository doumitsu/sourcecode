unit MultiDimensionalScaling;

interface

uses
  JacobiMethod;

(*古典的多次元尺度法*)
function pls_MultiDimensionalScaling(in_datN //データ数
                                     : integer;
                                     in_dat  //入力データ
                                     : array of double;
                                var  out_dat_result,
                                     //出力データ
                                     //（行：データ番号、列：地図化の軸番号）
                                     out_R,   //固有値
                                     out_A
                                     //固有ベクトルからなる行列
                                     //（行：データ番号、列：地図化の軸番号）
                                     : array of double;
                                     in_eps  //収束性を判定する規準
                                     : double;
                                     in_ct    //最大繰り返し回数
                                     : integer
                                     ) : integer; // result =0 : 正常
                                                  //        =1 : 収束せず

const
  datmax = 30;  //最大データ数

var
  C           //ヤコビ法に入力する行列
  : array[0..(datmax - 1) * (datmax - 1)] of double;

implementation

(*古典的多次元尺度法*)
function pls_MultiDimensionalScaling(in_datN //データ数
                                     : integer;
                                     in_dat  //入力データ
                                     : array of double;
                                var  out_dat_result,
                                     //出力データ
                                     //（行：データ番号、列：地図化の軸番号）
                                     out_R,   //固有値
                                     out_A
                                     //固有ベクトルからなる行列
                                     //（行：データ番号、列：地図化の軸番号）
                                     : array of double;
                                     in_eps  //収束性を判定する規準
                                     : double;
                                     in_ct    //最大繰り返し回数
                                     : integer
                                     ) : integer; // result =0 : 正常
                                                  //        =1 : 収束せず
var
  lop1, lop2, lop3, sw : integer;
  C0, C1, C2, Rtmp, Atmp : double;
  buf_dbl : array[0..datmax - 1] of double;
begin
  //ヤング･ハウスホルダーの変換//
  for lop1 := 0 to in_datN - 1 do begin
    for lop2 := 0 to in_datN - 1 do begin
      C0 := in_dat[in_datN * lop1 + lop2];
      C0 := (C0 * C0) / 2;
      C[in_datN * lop1 + lop2] := C0;
    end;
  end;
  C1 := 0;
  for lop1 := 0 to in_datN - 1 do begin
    C2 := 0;
    for lop2 := 0 to in_datN - 1 do begin
      C2 := C2 + C[in_datN * lop1 + lop2];
    end;
    C1 := C1 + C2;
    buf_dbl[lop1] := C2 / in_datN;  //buf_dbl : 行or列ごとの平均
  end;
  C1 := C1 / (in_datN * in_datN);   //A1 : 全平均
  for lop1 := 0 to in_datN - 1 do
    for lop2 := 0 to in_datN - 1 do
      C[in_datN * lop1 + lop2]
        := buf_dbl[lop1] + buf_dbl[lop2] - C1 - C[in_datN * lop1 + lop2];

  //ヤコビ法
  sw := pls_JacobiMethod(in_datN, in_ct, in_eps, C, out_R, out_A);

  if sw = 0 then begin
    //昇順並び替え//
    for lop1 := 0 to in_datN - 2 do begin
      for lop2 := lop1 + 1 to in_datN - 1 do begin
        if out_R[lop2] > out_R[lop1] then begin
          Rtmp := out_R[lop1];
          out_R[lop1] := out_R[lop2];
          out_R[lop2] := Rtmp;
          //固有ベクトルの番号を表す（列）方を入れ替える
          for lop3 := 0 to in_datN - 1 do begin
            Atmp := out_A[in_datN * lop3 + lop1];
            out_A[in_datN * lop3 + lop1] := out_A[in_datN * lop3 + lop2];
            out_A[in_datN * lop3 + lop2] := Atmp;
          end;
        end;
      end;
    end;
    //出力する位置の行列の計算//
    for lop1 := 0 to in_datN - 1 do
      for lop2 := 0 to in_datN - 1 do
        out_dat_result[in_datN * lop2 + lop1]
          := out_A[in_datN * lop2 + lop1] * sqrt(abs(out_R[lop1]));
  end;
  result := sw;
end;

end.
