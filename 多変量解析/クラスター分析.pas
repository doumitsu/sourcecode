unit ClusterAnalysis;

interface

uses
  CalcDistance;

procedure pls_ClusterAnalysis(in_method, //クラスター間距離の計算方法
                              in_clusN,  //生成するクラスターの数
                              in_datN,   //データの数
                              in_prmN    //変数の数
                              : integer;
                              in_dat     //データ
                              : array of double;
                          var out_clus,  //所属するグループ番号
                              out_post,
                              {Dendrogram作製用、データ並べ替え結果。
                              Post[k] = l なら場所kにlがいるということ}
                              out_post_buf,   //作業領域
                              out_ClusProc
                              {その組み合わせのデータがクラスター化された
                              ときの順番（何回目にクラスター化されたか）}
                              : array of integer;
                          var out_ClusDistance
                              {その組み合わせのデータがクラスター化された
                              ときのそのクラスター間の距離}
                              : array of double
                              );

implementation

const
  datmax = 1000;

var
  r : array[0..datmax * datmax - 1] of double;   //クラスター間距離


procedure pls_ClusterAnalysis(in_method, //クラスター間距離の計算方法
                              in_clusN,  //生成するクラスターの数
                              in_datN,   //データの数
                              in_prmN    //変数の数
                              : integer;
                              in_dat     //データ
                              : array of double;
                          var out_clus,  //所属するグループ番号
                              out_post,
                              {Dendrogram作製用、データ並べ替え結果。
                              Post[k] = l なら場所kにlがいるということ}
                              out_post_buf,   //作業領域
                              out_ClusProc
                              {その組み合わせのデータがクラスター化された
                              ときの順番（何回目にクラスター化されたか）}
                              : array of integer;
                          var out_ClusDistance
                              {その組み合わせのデータがクラスター化された
                              ときのそのクラスター間の距離}
                              : array of double
                              );

var
  lop1, lop2, lop3, ci, cj, M, k, CltCT, tmp_count : integer;
  NinClus  //クラスター内のデータ数。距離の計算用。
   : array[0..datmax - 1] of integer;
  min : double;
begin
  //初期設定
  M := in_datN;
  ci := 0;
  cj := 0;
  if in_method  < 4 then
    k := 0
  else
    k := 1;
  for lop1 := 0 to in_datN - 1 do begin
    out_clus[lop1] := lop1;
    NinClus[lop1] := 1;
    for lop2 := 0 to in_datN - 1 do
      r[in_datN * lop1 + lop2] := pls_range(k, lop1, lop2, in_prmN, in_dat);
  end;
  for lop1 := 0 to in_datN -1 do
    for lop2 := 0 to in_datN -1 do
      r[in_datN * lop2 + lop1] := r[in_datN * lop1 + lop2];
  CltCT := 1;
  while M > in_clusN do begin
    //最小距離のクラスターを探す
    min := -1;
    for lop1 := 0 to in_datN -1 do begin
      if out_clus[lop1] = lop1 then begin  //クラスターの先頭なら
        for lop2 := lop1 + 1 to in_datN -1 do begin
          if out_clus[lop2] = lop2 then begin  //次のクラスター
            if (min < 0) or (r[in_datN * lop1 + lop2] < min) then begin
              min := r[in_datN * lop1 + lop2];
              ci := lop1;
              cj := lop2;
            end;
          end;
        end;
      end;
    end;
    //他のクラスターとの距離を計算
    for lop1 := 0 to in_datN - 1 do begin
      if out_clus[lop1] = lop1 then begin  //クラスターの先頭なら
        for lop2 := lop1 + 1 to in_datN - 1 do begin
          if out_clus[lop2] = lop2 then begin   //クラスターの先頭なら
            if (lop1 <> cj) and (lop2 <> cj) then begin
              {クラスターciとクラスターcjが融合したクラスターと、
              他のクラスターとの距離を計算}
              if lop1 = ci then begin
                r[in_datN * lop1 + lop2] := pls_range_c(in_method, r[in_datN * ci +lop2],
                                           r[in_datN * cj + lop2], r[in_datN * ci + cj],
                                           NinClus[ci], NinClus[cj], NinClus[lop2]);
                r[in_datN * lop2 + lop1] := r[in_datN * lop1 + lop2];
              end else
              if lop2 = ci then begin
                r[in_datN * lop1 + lop2] := pls_range_c(in_method, r[in_datN * lop1 + ci],
                               r[in_datN * lop1 + cj], r[in_datN * ci + cj],
                               NinClus[ci], NinClus[cj], NinClus[lop2]);
                r[in_datN * lop2 + lop1] := r[in_datN * lop1 + lop2];
              end;
            end;
          end;
        end;
      end;
    end;
    {--ここから、Dendrogram用--}
    //クラスター化された組み合わせが何番目にクラスター化されたかを入力
    out_ClusProc[in_datN * ci + cj] := CltCT;
    out_ClusProc[in_datN * cj + ci] := CltCT;
    {クラスター化された組み合わせのそのときの距離を入力
    (そもそもrに保持されるものかも？cjに関しては融合以降変化しないから)}
    out_ClusDistance[in_datN * ci + cj] := r[in_datN * ci + cj];
    out_ClusDistance[in_datN * cj + ci] := r[in_datN * ci + cj];
    {場所の並べ替え用の配列変数out_post[]を操作(cjをciの次に移動させて、
    以降からcj直前までの分をNinClus[cj]個分後ろにずらす)}
    tmp_count := 0;
    for lop1 := 0 to in_datN - 1 do begin
      if out_post[lop1] = ci then begin  //場所lop1にciがいるなら
        for lop2 := 0 to in_datN - 1 do begin
          if out_post[lop2] = cj then begin  //場所lop2にcjがいるなら
                                              //注：ci < cj
            //クラスターCjを置換
            for lop3 := lop1 + NinClus[ci]
                  to lop1 + NinClus[ci] + NinClus[cj] - 1 do begin
              out_post[lop3] := out_post_buf[lop2 + tmp_count];
              inc(tmp_count);
            end;
            //置換した分、後ろにずらす
            for lop3 := lop1 + NinClus[ci] + NinClus[cj]
                  to lop2 + NinClus[cj] - 1 do
              out_post[lop3] := out_post_buf[lop3 - NinClus[cj]];
          end;
        end;
      end;
    end;
    for lop1 := 0 to in_datN - 1 do
      out_post_buf[lop1] := out_post[lop1];
    {--ここまで、Dendrogram用--}
    //クラスターの融合
    NinClus[ci] := NinClus[ci] + NinClus[cj];
    for lop1 := 0 to in_datN - 1 do begin
      if out_clus[lop1] = cj then
        out_clus[lop1] := ci;
    end;
    dec(M);
    inc(CltCT);
  end;
end;

end.
