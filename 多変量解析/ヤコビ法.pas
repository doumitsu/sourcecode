unit JacobiMethod;

interface

const
  prmmax = 50;     //入力パラメータ数の上限

var
  A1, A2,   //作業領域、A1の対角要素が固有値
  X1, X2    //作業領域、X1の各列が固有ベクトル
  : array[0..(prmmax - 1) * (prmmax - 1)] of double;

(*実対称行列の固有値・固有ベクトル（ヤコビ法）*)
function pls_JacobiMethod(in_N,    //次数
                          in_ct    //最大繰り返し回数
                          : integer;
                          in_eps   //収束判定条件
                          : double;
                          in_A     //対象とする行列
                          : array of double;
                     var  out_A,   //固有値の１次元配列
                          out_X    //各列が固有ベクトルの２次元配列
                          : array of double
                          ) : integer; //return = 0 正常
                                       //       = 1 収束せず

implementation


(*実対称行列の固有値・固有ベクトル（ヤコビ法）*)
function pls_JacobiMethod(in_N,    //次数
                          in_ct    //最大繰り返し回数
                          : integer;
                          in_eps   //収束判定条件
                          : double;
                          in_A    //対象とする行列
                          : array of double;
                     var  out_A,   //固有値の１次元配列
                          out_X    //各列が固有ベクトルの２次元配列
                          : array of double
                          ) : integer; //return = 0 正常
                                       //       = 1 収束せず

var
  max, s, t, v, sn, cs : double;
  k, p, q, lop1, lop2, ind : integer;
begin
  //初期設定//
  k := 0;
  p := 0;
  q := 0;
  ind := 1;
  for lop1 := 0 to in_N - 1 do begin
    for lop2 := 0 to in_N  - 1 do begin
      A1[in_N * lop1 + lop2] := in_A[in_N * lop1 + lop2];
      X1[in_N * lop1 + lop2] := 0;
    end;
    X1[in_N * lop1 + lop1] := 1;
  end;
  //計算//
  while (ind > 0) and (k < in_ct) do begin
    //最大要素の探索//
    max := 0;
    for lop1 := 0 to in_N - 1 do begin
      for lop2 := 0 to in_N - 1 do begin
        if lop1 <> lop2 then begin
          if abs(A1[in_N * lop1 + lop2]) > max then begin
            max := abs(A1[in_N * lop1 + lop2]);
            p := lop1;
            q := lop2;
          end;
        end;
      end;
    end;
    if max < in_eps then    //収束した場合
      ind := 0
    else begin           //収束しない場合
      //準備//
      s := - A1[in_N * p + q];
      t := 0.5 * (A1[in_N * p + p] - A1[in_N * q + q]);
      v := abs(t) / sqrt(s * s + t * t);
      sn := sqrt(0.5 * (1 - v));
      if (s * t) < 0 then
        sn := - sn;
      cs := sqrt(1 - sn * sn);
      //Akの計算//
      for lop1 :=0 to in_N - 1 do begin
        if lop1 = p then begin
          for lop2 := 0 to in_N - 1 do begin
            if lop2 = p then
              A2[in_N * p + p] := A1[in_N * p + p] * cs * cs
                                + A1[in_N * q + q] * sn * sn
                                - 2 * A1[in_N * p + q] * sn * cs
            else
              if lop2 = q then
                A2[in_N * p + q] := 0
              else
                A2[in_N * p + lop2] := A1[in_N * p + lop2] * cs
                                     - A1[in_N * q + lop2] * sn;
          end;
        end
        else if lop1 = q then begin
          for lop2 := 0 to in_N - 1 do begin
            if lop2 = q then
              A2[in_N * q + q] := A1[in_N * p + p] * sn * sn
                                + A1[in_N * q + q] * cs * cs
                                + 2 * A1[in_N * p + q] * sn * cs
            else
              if lop2 = p then
                A2[in_N * q + p] := 0
              else
                A2[in_N * q + lop2] := A1[in_N * q + lop2] * cs
                                     + A1[in_N * p + lop2] * sn;
          end;
        end
        else begin
          for lop2 := 0 to in_N - 1 do begin
            if lop2 = p then
              A2[in_N * lop1 + p] := A1[in_N * lop1 + p] * cs
                                   - A1[in_N * lop1 + q] * sn
            else
              if lop2 = q then
                A2[in_N * lop1 + q] := A1[in_N * lop1 + q] * cs
                                     + A1[in_N * lop1 + p] * sn
              else
                A2[in_N * lop1 + lop2] := A1[in_N * lop1 + lop2];
          end;
        end;
      end;
      //Xkの計算//
      for lop1 := 0 to in_N - 1 do begin
        for lop2 := 0 to in_N - 1 do begin
          if lop2 = p then
            X2[in_N * lop1 + p] := X1[in_N * lop1 + p] * cs
                                 - X1[in_N * lop1 + q] * sn
          else
            if lop2 = q then
              X2[in_N * lop1 + q] := X1[in_N * lop1 + q] * cs
                                   + X1[in_N * lop1 + p] * sn
            else
              X2[in_N * lop1 + lop2] := X1[in_N * lop1 + lop2];
        end;
      end;
      Inc(k);
      for lop1 := 0 to in_N - 1 do begin
        for lop2 := 0 to in_N - 1 do begin
          A1[in_N * lop1 + lop2] := A2[in_N * lop1 + lop2];
          X1[in_N * lop1 + lop2] := X2[in_N * lop1 + lop2];
        end;
      end;
    end;
  end;
  //収束した場合、出力変数へ//
  if ind = 0 then begin
    for lop1 := 0 to in_N - 1 do begin
      out_A[lop1] := A1[in_N * lop1 + lop1];
      for lop2 := 0 to in_N - 1 do begin
        out_X[in_N * lop1 + lop2] := X1[in_N * lop1 + lop2];
      end;
    end;
  end;
  result := ind;
end;

end.
